package main

import (
	"flag"
	"fmt"
	"os"
)

// shamelessly snagged from the go tool
// each command gets its own set of args,
// defines its own entry point, and provides its own help
type Command struct {
	Run  func(cmd *Command, args ...string)
	Flag flag.FlagSet

	Name  string
	Usage string

	Summary string
	Help    string

	triggerHelp *bool
}

func (c *Command) Exec(args []string) {
	c.Flag.Usage = func() {
		// helpFunc(c, c.Name)
	}
	c.triggerHelp = c.Flag.Bool("help", false, "Show help")
	c.Flag.Parse(args)

	if *c.triggerHelp {
		c.PrintUsage()
	}

	c.Run(c, c.Flag.Args()...)
}

func (c *Command) PrintUsage() {
	fmt.Fprintf(os.Stderr, "Usage:\n\n    goose %s\n\n", c.Usage)
	if c.Help != "" {
		fmt.Fprintf(os.Stderr, "Help:\n\n%s\n\n", c.Help)
	}
	os.Exit(2)
}
